#seeds

MenuItem.create!([
  {item_name: "Strawberry Smoothie", price: 2.0},
  {item_name: "Mocha Latte", price: 4.0},
  {item_name: "Vanilla Latte", price: 4.0}
])
Order.create!([
  {name: "Eli", subtotal: "2.0", total: "2.2", menu_item_id: 9, tax: "0.2", date_of_sale: "2018-05-01", user_id: nil},
  {name: "Yvonne", subtotal: "4.0", total: "4.4", menu_item_id: 11, tax: "0.4", date_of_sale: "2018-05-26", user_id: nil},
  {name: "Tomas", subtotal: "4.0", total: "4.4", menu_item_id: 10, tax: "0.4", date_of_sale: "2018-05-01", user_id: nil},
  {name: "Lancaster", subtotal: "4.0", total: "4.4", menu_item_id: 10, tax: "0.4", date_of_sale: "2018-03-03", user_id: nil}
])
Position.create!([
  {position: "Manager"},
  {position: "Barista"},
  {position: "Manager"},
  {position: "Barista"},
  {position: "Manager"},
  {position: "Barista"}
])
User.create!([
  {user_id: 1, first_name: "Jenny", last_name: "Garcia", position_id: 1},
  {user_id: 2, first_name: "Yvonne", last_name: "Alvarado", position_id: 2},
  {user_id: 3, first_name: "Elisha", last_name: "Diaz", position_id: 2}
])
