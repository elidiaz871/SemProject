class AddDateOfSaletoOrders < ActiveRecord::Migration[5.1]
  def change
    add_column :orders, :date_of_sale, :date
  end
end
