class AddTaxRefToOrder < ActiveRecord::Migration[5.1]
  def change
    add_column :orders, :tax, :decimal
  end
end
