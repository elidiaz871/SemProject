class Order < ApplicationRecord
  belongs_to :menu_item
  validates :menu_item, presence: true
  after_save :calculate_total
  accepts_nested_attributes_for :menu_item,
                                reject_if: :all_blank, allow_destroy: true

  #what is used to filter, scope is a method, find the dates in range
  # checks for both start and end
  scope :in_range, ->(start_date, end_date) { where("date_of_sale >= ? AND date_of_sale <= ?", start_date, end_date) }
  def calculate_total
    subtotal = menu_item.price
    tax = subtotal / 10
    total = tax + subtotal
    update_columns(subtotal: subtotal, tax: tax, total: total)
  end
end
