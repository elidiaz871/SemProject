class User < ApplicationRecord
  has_many :orders
  belongs_to :position
  validates :user_id, uniqueness: true, numericality: { only_integer: true, greater_than: 0}

  #function to join firstname last name into single string
  # makes an array, selects the two things if present, and then joins them and sperates them with
  # a space, which is the ''
  def name
    [first_name, last_name].select(&:present?).join(' ')
  end
end
