Rails.application.routes.draw do

  #added a route called filter, to filter the orders based on different
  # parameters, so whatever parameter we pass, we can filter them
  post :filter, to: 'orders#filter'
  #making orders pages the home page
  root 'orders#index'

  resources :positions
  resources :orders
  resources :menu_items
  resources :users




  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
